CXXFLAGS= -Wall -Wextra -ggdb -std=c++11 `pkg-config --cflags libnotifymm-1.0`
LIBS 	= -lstdc++ `pkg-config --libs libnotifymm-1.0` -lboost_filesystem -lboost_system

TARGET 	= notify

.PHONY: all
all: 	$(TARGET)

notify: notify.cc
	$(CC) $(CXXFLAGS) -o notify notify.cc $(LIBS)

