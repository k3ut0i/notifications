#include <libnotifymm-1.0/libnotifymm.h>
#include <iostream>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/progress.hpp>

namespace fs = boost::filesystem;
int main(int argc, char *argv[]) {
    const std::string maildirs = "/home/keutoi/.mail/";
    fs::path p(maildirs);
    if(fs::exists(p))
    {
    if(fs::is_directory(p)){
        std::cout << "in the directory" << p  << std::endl;
        fs::directory_iterator end_itr;
        int dir_count;
        int file_count;
        for(fs::directory_iterator dir_itr(p);dir_itr != end_itr; ++dir_itr)
        {
            try{
                if(fs::is_directory(dir_itr->status())){
                    std::cout << "found mail directory\t" << dir_itr->path().filename() << std::endl;
                    //go into the directory/INBOX/new
                    //  and check for new mails
                    //      if mail exsists fetch their subjects and to, from info
                    //      use lib mimetic
                }
                if(fs::is_regular_file(dir_itr->status())) std::cout << "found irregular file\t" << dir_itr->path().filename() << std::endl;
                }
            catch(...)
                {
                    std::cerr << "Caught exception. Exiting \n";
                    exit(12);
                }

        }
        }
    Notify::init("Hello world!");
    Notify::Notification Hello("Hello world", "This is an example notification.", "dialog-information");
        Hello.show();
    }

    return 0;
}
